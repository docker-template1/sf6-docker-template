ARG PHP_VERSION
ARG PHP_ALPINE_VERSION
FROM php:${PHP_VERSION}RC1-fpm-alpine${PHP_ALPINE_VERSION}
#FROM php:8.3.3RC1-fpm-alpine3.19
LABEL author="Lacina DIANE"
RUN addgroup -g 1000 app && adduser -u 1000 -s /bin/sh -G app -S app
WORKDIR /home/app
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions sodium gd xdebug yaml amqp redis pdo_pgsql intl opcache
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && php -r "unlink('composer-setup.php');" && mv composer.phar /usr/local/bin/composer
RUN apk add --no-cache bash && curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash && \
    apk add --no-cache symfony-cli git && symfony server:ca:install
USER app
COPY --chown=app:app . .
RUN chmod u+x docker-entrypoint.sh
EXPOSE 8000
EXPOSE 443
#ENTRYPOINT ./docker-entrypoint.sh